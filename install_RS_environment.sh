#!/usr/bin/env bash

###########################
# This script installs the dotfiles and runs all other system configuration scripts
# @author Emilien Alvarez-Vanhard
###########################

sudo apt update
sudo apt upgrade


sudo apt install python-pip

sudo pip install --upgrade pip

# Git

sudo apt install git

# Disk utility

sudo apt install gnome-disk-utility

# Owncloud Mycore CNRS

sudo apt install curl
echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/desktop/Ubuntu_18.04/ /' | sudo tee /etc/apt/sources.list.d/isv:ownCloud:desktop.list
curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:desktop/Ubuntu_18.04/Release.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/isv:ownCloud:desktop.gpg > /dev/null
sudo apt update
sudo apt install owncloud-client

sudo rm /etc/apt/trusted.gpg.d/isv:ownCloud:desktop.gpg #supprime vieille clé de owncloud

#Zotero

sudo add-apt-repository ppa:smathot/cogscinl
sudo apt update
sudo apt install zotero-standalone

# Remmina

sudo apt-add-repository ppa:remmina-ppa-team/remmina-next
sudo apt-get update
sudo apt-get install remmina remmina-plugin-rdp

# KeepassXC
sudo apt-get update
sudo apt install keepassxc

# RSenv : ODC, QGIS, OTB
# Miniconda3

wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod +x Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh -p $HOME/miniconda3

conda config --set auto_activate_base false
conda config --add channels conda-forge

conda create --name RSenv python=3.6 datacube qgis jupyter matplotlib scipy
mkdir ~/RSenv/

# Add external packages to conda environment

conda activate RSenv
conda env config vars set PYTHONPATH=/home/alvarez-vanhard_e/RSenv/ODC/ODC_notebooks/gefolki

# Jupyterlab

conda activate RSenv
conda install jupyterlab

conda install -c conda-forge nodejs=12

conda install -c conda-forge jupyterlab-git

conda install folium
pip install sidecar

jupyter labextension install @jupyter-widgets/jupyterlab-manager
jupyter labextension install @jupyter-widgets/jupyterlab-sidecar
conda install -c conda-forge ipyleaflet
jupyter labextension install @jupyter-widgets/jupyterlab-manager jupyter-leaflet
jupyter labextension install jupyterlab-drawio
jupyter labextension install spreadsheet-editor
jupyter labextension install @jupyterlab/toc


# OpendataCube / PostgreSQL

sudo apt install postgresql-10 postgresql-client-10 postgresql-contrib-10
sudo -u postgres psql postgres
#\password postgres

createdb -h localhost -U postgres datacube
nano ~/.datacube.conf

conda activate RSenv
datacube -v system init

# Orfeo Tool Box (python => 3.7)

conda install -c orfeotoolbox otb

# Pandoc s'intalle avec Jupyter mais une version plus récente peut-être téléchargé sur leur site.
